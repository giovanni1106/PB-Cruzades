using System;
using System.Collections;
using UnityEngine;
using TMPro;


public class Manager : MonoBehaviour
{
    private Anim ImageLeft;
    private Anim ImageCenter;
    private Anim ImageRight;
    private Anim Hand;
    private Anim Circle1;
    private Anim Circle2;
    private Anim Touch;

    private GameObject[] Imagens = new GameObject[10];

    private Click Button;
    private Click ButtonArabic;
    private Click ButtonEnglish;
    private Click ButtonHebrew;
    private Click ButtonLeft;
    private Click ButtonRight;

    private int ImageNow;
    private GameObject TextArabic;
    private GameObject TextEnglish;
    private GameObject TextHebrew;

    private GameObject Line1;
    private GameObject Line2;
    private GameObject Line3;

    private Camera Cam;

    public static int conjunto = 1;
    public static int Idioma = 3;
    public static int cont = 0;

    public static bool Clicar = false;
    public static bool Permissao = true;
    public static bool Left = false;
    public static bool Right = false;

    Vector3 nulo = new Vector3(0, 0, 0);
    Vector3 Clicado = new Vector3(0.1f, 0, 0);
    Vector3 toqueTela1 = new Vector3(0, 0, 0);
    Vector3 toqueTela2 = new Vector3(0, 0, 0);

    void Start()
    {
        // -------------- Declara as variaveis

        // Telas
        Imagens[0] = GameObject.Find("Tela1");
        Imagens[1] = GameObject.Find("Tela2");
        Imagens[2] = GameObject.Find("Tela3");
        Imagens[3] = GameObject.Find("Tela4");
        Imagens[4] = GameObject.Find("Tela5");
        Imagens[5] = GameObject.Find("Tela6");

        // Imagens atuais
        ImageNow = 0;

        ImageLeft = Imagens[5].transform.Find("Image").GetComponent<Anim>();
        ImageCenter = Imagens[ImageNow].transform.Find("Image").GetComponent<Anim>();
        ImageRight = Imagens[ImageNow + 1].transform.Find("Image").GetComponent<Anim>();

        // Textos
        TextArabic = Imagens[ImageNow].transform.Find("TextArabic").gameObject;
        TextEnglish = Imagens[ImageNow].transform.Find("TextEnglish").gameObject;
        TextHebrew = Imagens[ImageNow].transform.Find("TextHebrew").gameObject;
        
        // Botes
        Button = GameObject.Find("Button").GetComponent<Click>();
        ButtonArabic = GameObject.Find("ButtonArabic").GetComponent<Click>();
        ButtonEnglish = GameObject.Find("ButtonEnglish").GetComponent<Click>();
        ButtonHebrew = GameObject.Find("ButtonHebrew").GetComponent<Click>();
        ButtonLeft = GameObject.Find("ButtonLeft").GetComponent<Click>();
        ButtonRight = GameObject.Find("ButtonRight").GetComponent<Click>();

        // Mao
        Hand = GameObject.Find("Hand").GetComponent<Anim>();
        Circle1 = GameObject.Find("Circle1").GetComponent<Anim>();
        Circle2 = GameObject.Find("Circle2").GetComponent<Anim>();
        Touch = GameObject.Find("Touch").GetComponent<Anim>();

        // Realiza a primeira animao do software
        ImageCenter.FirstTime();

        // Chama o loop
        StartCoroutine(Loop());
    }

    void Update()
    {
        // Atualiza a legenda de acordo com a imagem
        RefreshText();

        Vector3 result = Clicado;

        // ----- Recebe a posio do mouse -----
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePosition = Input.mousePosition;
            toqueTela1 = mousePosition;
        }
        if (Input.GetMouseButtonUp(0))
        {
            Vector3 mousePosition = Input.mousePosition;
            toqueTela2 = mousePosition;
        }
        // --------------------------------------

        if (Clicar == true && Permissao == true)
        {
            if (ButtonLeft.Clicar == true)
            {
                ButtonLeft.Clicar = false;
                Vector3 Move = new Vector3(-200, 0, 0);
                Drag(Move);
            }

            if (ButtonRight.Clicar == true)
            {
                ButtonRight.Clicar = false;
                Vector3 Move = new Vector3(200, 0, 0);
                Drag(Move);
            }
        }

        // Time.deltaTime();

        // Envia movimentao na tela
        if (Permissao == true && toqueTela2 != nulo)
        {
            result = toqueTela1 - toqueTela2;
            Drag(result);

            toqueTela1 = nulo;
            toqueTela2 = nulo;
        }

        // Zera os vetores caso n seja permitido novo toque
        if (Permissao == false)
        {
            ButtonLeft.Clicar = false;
            ButtonRight.Clicar = false;
        }

        // Intercepta click na tela
        if (Button.Clicar == true && Math.Abs(result.x) < Math.Abs(Clicado.x))
        {
            if (Clicar == false)
                Clicar = true;
            else
                Clicar = false;
        }
        else
            Button.Clicar = false;
    }

    public void Drag(Vector3 result)
    {
        Permissao = false;

        if (result.x > 25)
        {
            cont = 0; // Reseta o timer para Stand By
            Left = true;
        }
        else if (result.x < -25)
        {
            cont = 0; // Reseta o timer para Stand By
            Right = true;
        }
    }

    public IEnumerator Loop()
    {
        Touch.TouchOn();

        // Tempo para mudar imagem do Stand By
        for (int i = 0; i < 60; i++)
            if (Clicar == false && Right == false && Left == false)
            {
                yield return new WaitForSeconds(0.1f);

                if (i == 20)
                    StartHand();
            }
            else
                break;

        while (true)
        {
            if (Clicar == false)
            {
                Touch.TouchOn();

                // Animao para StandBy
                if (Right == false && Left == false)
                    StandBy();
                else
                {
                    if (Right == true)
                    {
                        ImageCenter.StandByRightOn();
                        ImageLeft.StandByRightOff();
                        RefreshImage();
                    }
                    else if (Left == true)
                    {
                        StandBy();
                    }
                }

                yield return new WaitForSeconds(0.2f);
                Permissao = true;

                // Tempo para a animao
                yield return new WaitForSeconds(1);
            }
            else // ------------------------------ Caso o software entre em uso -------------------
            {
                Touch.TouchOff();

                // Animao para click na tela
                FirstClick();


                ButtonHebrew.On();
                yield return new WaitForSeconds(0.1f);
                ButtonEnglish.On();
                yield return new WaitForSeconds(0.1f);
                ButtonArabic.On();
                
                

                yield return new WaitForSeconds(0.6f);

                ShowLines();
                ButtonLeft.LeftOn();
                ButtonRight.RightOn();

                // Tempo para a animao
                yield return new WaitForSeconds(1);

                // Permissao para receber movimentao na tela
                Permissao = true;

                while (cont <= 50) // Tempo para voltar ao Stand By
                {
                    // Soma o contator enquanto no h movimentao na tela
                    cont++;

                    if (ButtonArabic.Clicar == true)
                    {
                        if (ChangeLanguages(1))
                        {
                            yield return new WaitForSeconds(0.5f);
                            ShowText();
                        }

                        yield return new WaitForSeconds(1);

                        ButtonArabic.Clicar = false;
                        ButtonEnglish.Clicar = false;
                        ButtonHebrew.Clicar = false;

                        cont = 0;
                    }

                    if (ButtonEnglish.Clicar == true)
                    {
                        if (ChangeLanguages(2))
                        {
                            yield return new WaitForSeconds(0.5f);
                            ShowText();
                        }

                        yield return new WaitForSeconds(1);

                        ButtonArabic.Clicar = false;
                        ButtonEnglish.Clicar = false;
                        ButtonHebrew.Clicar = false;

                        cont = 0;
                    }

                    if (ButtonHebrew.Clicar == true)
                    {
                        if (ChangeLanguages(3))
                        {
                            yield return new WaitForSeconds(0.5f);
                            ShowText();
                        }

                        yield return new WaitForSeconds(1);

                        ButtonArabic.Clicar = false;
                        ButtonEnglish.Clicar = false;
                        ButtonHebrew.Clicar = false;

                        cont = 0;
                    }

                    if (Left == true)
                    {
                        HideText();

                        ImageCenter.LeftOn();
                        ImageRight.LeftOff();
                        RefreshImage();

                        yield return new WaitForSeconds(1.5f);
                        ShowLines();

                        // Tempo para a animao
                        yield return new WaitForSeconds(1.5f);
                    }

                    if (Right == true)
                    {
                        HideText();

                        ImageCenter.RightOn();
                        ImageLeft.RightOff();
                        RefreshImage();

                        yield return new WaitForSeconds(1.5f);
                        ShowLines();

                        // Tempo para a animao
                        yield return new WaitForSeconds(1.5f);
                    }

                    // Liberado para receber nova movimentao
                    Permissao = true;

                    // Soma do tempo para Stand By
                    yield return new WaitForSeconds(0.25f);

                    if (Clicar == false)
                        cont = 51;
                }

                // Voltando ao modo de Stand By
                ReturnStandBy();
                Touch.TouchOn();

                // Esperando animao
                yield return new WaitForSeconds(1.5f);
                ChangeLanguages(3);

            }

            Permissao = true;

            // Tempo para mudar imagem do Stand By
            for (int i = 0; i < 60; i++)
                if (Clicar == false && Right == false && Left == false)
                {
                    yield return new WaitForSeconds(0.1f);

                    if (i == 20)
                        StartHand();
                }
                else
                    break;
        }
    }

    public void StandBy()
    {
        ImageCenter.StandByLeftOn();
        ImageRight.StandByLeftOff();
        RefreshImage();
    }

    public void FirstClick()
    {
        ImageCenter.CLK();

        switch (Idioma)
        {
            case 1:
                ButtonArabic.transform.GetChild(0).GetComponent<Anim>().BackgroundOff();
                break;
            case 2:
                ButtonEnglish.transform.GetChild(0).GetComponent<Anim>().BackgroundOff();
                break;
            case 3:
                ButtonHebrew.transform.GetChild(0).GetComponent<Anim>().BackgroundOff();
                break;
        }
    }

    public void ShowLines()
    {
        switch (Idioma)
        {
            case 1:
                TextArabic.GetComponent<text>().On();
                Line1 = TextArabic.transform.Find("Mask1").transform.GetChild(0).gameObject;
                Line2 = TextArabic.transform.Find("Mask2").transform.GetChild(0).gameObject;
                Line3 = TextArabic.transform.Find("Mask3").transform.GetChild(0).gameObject;
                break;
            case 2:
                TextEnglish.GetComponent<text>().On();
                Line1 = TextEnglish.transform.Find("Mask1").transform.GetChild(0).gameObject;
                Line2 = TextEnglish.transform.Find("Mask2").transform.GetChild(0).gameObject;
                Line3 = TextEnglish.transform.Find("Mask3").transform.GetChild(0).gameObject;
                break;
            case 3:
                TextHebrew.GetComponent<text>().On();
                Line1 = TextHebrew.transform.Find("Mask1").transform.GetChild(0).gameObject;
                Line2 = TextHebrew.transform.Find("Mask2").transform.GetChild(0).gameObject;
                Line3 = TextHebrew.transform.Find("Mask3").transform.GetChild(0).gameObject;
                break;
        }

        Line1.GetComponent<text>().Line1On();
        Line2.GetComponent<text>().Line2On();
        Line3.GetComponent<text>().Line3On();
    }

    public void HideText()
    {
        switch (Idioma)
        {
            case 1:
                TextArabic.GetComponent<text>().Off();
                break;
            case 2:
                TextEnglish.GetComponent<text>().Off();
                break;
            case 3:
                TextHebrew.GetComponent<text>().Off();
                break;
        }
    }

    public void ShowText()
    {
        switch (Idioma)
        {
            case 1:
                TextArabic.GetComponent<text>().On();
                break;
            case 2:
                TextEnglish.GetComponent<text>().On();
                break;
            case 3:
                TextHebrew.GetComponent<text>().On();
                break;
        }
    }

    public void RefreshText()
    {
        TextArabic = Imagens[ImageNow].transform.Find("TextArabic").gameObject;
        TextEnglish = Imagens[ImageNow].transform.Find("TextEnglish").gameObject;
        TextHebrew = Imagens[ImageNow].transform.Find("TextHebrew").gameObject;
    }

    public void RefreshImage()
    {
        if (Right == true)
        {
            if (ImageNow != 0)
                ImageNow--;
            else
                ImageNow = 5;
        }
        else
        {
            if (ImageNow != 5)
                ImageNow++;
            else
                ImageNow = 0;
        }

        if (ImageNow == 0)
            ImageLeft = Imagens[5].transform.Find("Image").GetComponent<Anim>();
        else
            ImageLeft = Imagens[ImageNow - 1].transform.Find("Image").GetComponent<Anim>();

        ImageCenter = Imagens[ImageNow].transform.Find("Image").GetComponent<Anim>();

        if (ImageNow == 5)
            ImageRight = Imagens[0].transform.Find("Image").GetComponent<Anim>();
        else
            ImageRight = Imagens[ImageNow + 1].transform.Find("Image").GetComponent<Anim>();

        Right = false;
        Left = false;
    }

    public bool ChangeLanguages(int Button)
    {
        /*
         * Idioma = Idioma atual
         * Button = Idioma escolhido
         * 
         * 1- Arabic
         * 2- English
         * 3- Hebrew
         */

        if (Idioma != Button)
        {
            switch (Idioma)
            {
                case 1:
                    ButtonArabic.transform.GetChild(0).GetComponent<Anim>().BackgroundOn();
                    break;
                case 2:
                    ButtonEnglish.transform.GetChild(0).GetComponent<Anim>().BackgroundOn();
                    break;
                case 3:
                    ButtonHebrew.transform.GetChild(0).GetComponent<Anim>().BackgroundOn();
                    break;
            }

            HideText();
            Idioma = Button;

            switch (Idioma)
            {
                case 1:
                    ButtonArabic.transform.GetChild(0).GetComponent<Anim>().BackgroundOff();
                    break;
                case 2:
                    ButtonEnglish.transform.GetChild(0).GetComponent<Anim>().BackgroundOff();
                    break;
                case 3:
                    ButtonHebrew.transform.GetChild(0).GetComponent<Anim>().BackgroundOff();
                    break;
            }

            return true;
        }
        return false;
    }

    public void ReturnStandBy()
    {
        ImageCenter.NoCLK();

        switch (Idioma)
        {
            case 1:
                TextArabic.GetComponent<text>().Off();
                break;
            case 2:
                TextEnglish.GetComponent<text>().Off();
                break;
            case 3:
                TextHebrew.GetComponent<text>().Off();
                break;
        }

        ButtonArabic.Off();
        ButtonEnglish.Off();
        ButtonHebrew.Off();

        ButtonLeft.LeftOff();
        ButtonRight.RightOff();

        Button = GameObject.Find("Button").GetComponent<Click>();
        Button.Clicar = false;
        Clicar = false;
        Left = false;
        Right = false;

        Permissao = true;

        cont = 0;
    }

    public void StartHand()
    {
        Hand.Hand();
        Circle1.Circle1();
        Circle2.Circle2();
    }
}
