using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class text : MonoBehaviour
{
    Animation Anim;

    public void Line1On()
    {
        Anim = GetComponent<Animation>();
        Anim.Play("Line1On");
    }

    public void Line2On()
    {
        Anim = GetComponent<Animation>();
        Anim.Play("Line2On");
    }

    public void Line3On()
    {
        Anim = GetComponent<Animation>();
        Anim.Play("Line3On");
    }

    public void Off()
    {
        Anim = GetComponent<Animation>();
        Anim.Play("TextOff");
    }

    public void On()
    {
        Anim = GetComponent<Animation>();
        Anim.Play("TextOn");
    }
}
