using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Click : MonoBehaviour
{
    public Button ButtonThree;
    public bool Clicar = false;
    Animation Anim;

    private void Start()
    {
        Button btn1 = ButtonThree.GetComponent<Button>();
        btn1.onClick = new Button.ButtonClickedEvent();
        btn1.onClick.AddListener(() => Ativo());
    }
    public void Ativo()
    {
            Clicar = true;
    }

    public void On()
    {
        Anim = GetComponent<Animation>();
        Anim.Play("ButtON");
    }

    public void Off()
    {
        Anim = GetComponent<Animation>();
        Anim.Play("ButtOFF");
    }

    public void LeftOn()
    {
        Anim = GetComponent<Animation>();
        Anim.Play("ButtonLeftOn");
    }

    public void LeftOff()
    {
        Anim = GetComponent<Animation>();
        Anim.Play("ButtonLeftOff");
    }

    public void RightOn()
    {
        Anim = GetComponent<Animation>();
        Anim.Play("ButtonRightOn");
    }

    public void RightOff()
    {
        Anim = GetComponent<Animation>();
        Anim.Play("ButtonRightOff");
    }
}
