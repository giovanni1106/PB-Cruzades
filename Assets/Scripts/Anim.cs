using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Anim : MonoBehaviour
{
    Animation Anime;

    public void FirstTime()
    {
        Anime = GetComponent<Animation>();
        Anime.Play("FirstTime");
    }

    public void StandByLeftOn()
    {
        Anime = GetComponent<Animation>();
        Anime.Play("StandBy");
    }

    public void StandByLeftOff()
    {
        Anime = GetComponent<Animation>();
        Anime.Play("StandBy2");
    }


    public void StandByRightOn()
    {
        Anime = GetComponent<Animation>();
        Anime.Play("StandByRight");
    }

    public void StandByRightOff()
    {
        Anime = GetComponent<Animation>();
        Anime.Play("StandByRight2");
    }


    public void CLK()
    {
        Anime = GetComponent<Animation>();
        Anime.Play("click");
    }

    public void NoCLK()
    {
        Anime = GetComponent<Animation>();
        Anime.Play("noClick");
    }

    public void LeftOn()
    {
        Anime = GetComponent<Animation>();
        Anime.Play("MoveLeft");
    }

    public void LeftOff()
    {
        Anime = GetComponent<Animation>();
        Anime.Play("MoveLeft2");
    }

    public void RightOn()
    {
        Anime = GetComponent<Animation>();
        Anime.Play("MoveRight");
    }

    public void RightOff()
    {
        Anime = GetComponent<Animation>();
        Anime.Play("MoveRight2");
    }

    public void BackgroundOn()
    {
        Anime = GetComponent<Animation>();
        Anime.Play("BackgroundOn");
    }

    public void BackgroundOff()
    {
        Anime = GetComponent<Animation>();
        Anime.Play("BackgroundOff");
    }

    public void Hand()
    {
        Anime = GetComponent<Animation>();
        Anime.Play("Hand");
    }
    

    public void Circle1()
    {
        Anime = GetComponent<Animation>();
        Anime.Play("Circle1");
    }

    public void Circle2()
    {
        Anime = GetComponent<Animation>();
        Anime.Play("Circle2");
    }

    public void TouchOff()
    {
        Anime = GetComponent<Animation>();
        Anime.Play("TouchOff");
    }

    public void TouchOn()
    {
        Anime = GetComponent<Animation>();
        Anime.Play("TouchOn");
    }

    public void OndaOn()
    {
        Anime = GetComponent<Animation>();
        Anime.Play("OndaOn");
    }

    public void IconOn()
    {
        Anime = GetComponent<Animation>();
        Anime.Play("Icon1");
    }
}
